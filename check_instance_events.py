#!/usr/bin/env python
"""
Determine if instances in a given region have pending events.
"""

import getopt,os,sys,warnings
os.environ['BOTO_CONFIG'] = '/usr/lib64/nagios/plugins/boto_config'
import boto.ec2


def usage(message=None, do_exit=False, exit_code=3):
    """
    Print the usage of the program, with optional message
    """
    usage_string = """Usage: %s (options)

Options:

--help   Show help
--region Region instance should be in (default: any region)
--name   Name filter for instance(s) (you can use * in the name to glob)
--verbose
""" % (__file__ )
    if message:
        print('\n'.join([message, usage_string]))
    else:
        print(usage_string)
    if do_exit:
       sys.exit(exit_code) 
# End usage

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:], '', ['help', 'region=', 'name=', 'verbose'])
    except getopt.GetoptError, err:
        print(str(err))
        usage(do_exit=True)

    # Process command parameters
    params = dict([(x.lstrip('-'), y) for x, y in opts]) # Strip out '--' from option name please
    if 'help' in params:
        usage(do_exit=True)

    if 'region' in params:
        regions = [params['region']]
    else:
        regions = [region.name for region in boto.ec2.regions()]

    if 'verbose' in params:
        params['verbose'] = True
    else:
        params['verbose'] = False

    # EC2 connection objects for the regions
    region_connections = {}
    for region_name in regions:
        try:
            connection = boto.ec2.connect_to_region(region_name)
            region_connections[region_name] = connection
        except boto.exception.EC2ResponseError as e:
            if e.error_code == 'AuthFailure':
                if params['verbose']: warnings.warn('Error connecting to region %s: %s' % (region, e.message))
                continue

    pending_events = [] # List of pending events for instances

    for region_name, conn in region_connections.items():
        if params['verbose']: print('Checking region %s' % (region_name))
        # Get a list of instances in each region
        try:
            if 'name' in params:
                instance_statuses = conn.get_all_instance_status(filters={'tag:Name': params['name']}, include_all_instances=True)
            else:
                instance_statuses = conn.get_all_instance_status(include_all_instances=True)
        except boto.exception.EC2ResponseError as e:
            if e.error_code == 'AuthFailure':
                if params['verbose']: warnings.warn('I am not allowed to operate in region %s: %s' % (region_name, e.message))
                continue

        # Check for pending events, for each instance
        for status in instance_statuses:
            # NOTE: status.events is None if there are no pending events (as opposed to an empty list)
            if (status.events and
                filter(lambda x: not x.description.startswith('[Completed]'), status.events)):
                # There are pending (incomplete) events, so alarm
                #event_string = ','.join(['event_code:%s desc:"%s" not_before:%s not_after:%s' % (event.code, event.description, event.not_before, event.not_after)for event in status.events])
                
                instance = conn.get_only_instances(instance_ids=status.id)[0]
                instance_name = ''
                if 'Name' in instance.tags:
                    instance_name = instance.tags['Name']

                message = 'name=%s id=%s zone=%s' % (
                    instance_name, status.id, status.zone)
                    #status.state_name, event_string)

                if params['verbose']:
                    print(message)
                pending_events.append(message)

    if pending_events:
        print('AWS CRITICAL: Pending events for %d instances! | %s' % (
            len(pending_events), '\n'.join(pending_events)))
        sys.exit(2)
    else:
        print('AWS OK: No pending events found.')
# End main
